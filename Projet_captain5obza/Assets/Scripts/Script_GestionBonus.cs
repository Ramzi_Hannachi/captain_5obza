﻿using UnityEngine;
using System.Collections;

public class Script_GestionBonus : MonoBehaviour 
{
	public static int _random=0;
	//
	public Transform _bonusMouthabra_2;
	public Transform _bonusHobwatan_4;
	public Transform _bonuspine_1;
	public Transform _bonussede9_3;
	//
	void Start () 
	{
	
	}
	
	public void Fonction_GestionBonus(int random,Transform _transform)
	{
		_random = random;
		Debug.Log ("random:"+_random);
		if(random == 1)
		{
			Script_Invocation.InvocationObject(_bonusMouthabra_2,_transform);
		}
		if(random == 2)
		{
			Script_Invocation.InvocationObject(_bonusHobwatan_4,_transform);
		}
		if(random == 3)
		{
			Script_Invocation.InvocationObject(_bonuspine_1,_transform);
		}
		if(random == 4)
		{
			Script_Invocation.InvocationObject(_bonussede9_3,_transform);
		}
	}

}
