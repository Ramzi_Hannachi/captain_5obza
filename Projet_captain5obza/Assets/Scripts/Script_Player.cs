﻿using UnityEngine;
using System.Collections;

public class Script_Player : MonoBehaviour 
{
	public Vector2 _speed = new Vector2(10,10);

	private Vector2 _mouvement;
	private ParticleSystem _superPouvoir;
	private float _tempsAttente = 6f;
	private float _tempsEcouler;
	private bool _BTsuperPouvoir;

	void Awake()
	{
		_superPouvoir = gameObject.GetComponentInChildren<ParticleSystem>();
	}
	// Use this for initialization
	void Start () 
	{
		_tempsEcouler = 0f;
	}
	// Update is called once per frame
	void Update () 
	{
		//print("_tempsEcouler: "+_tempsEcouler);
		if (_tempsEcouler > 0) 
		{
			_tempsEcouler -= Time.deltaTime;
		}
		if(_tempsEcouler <= 0)
		{
			//print("ok");
			_superPouvoir.enableEmission = false;
		}
		// deplacement
		float _InputX = Input.GetAxis ("Horizontal");
		float _InputY = Input.GetAxis ("Vertical");
		_mouvement = new Vector2 (_InputX * _speed.x , _InputY * _speed.y);
		// feux
		bool _Fire = Input.GetButtonDown ("Fire1");
		_Fire = Input.GetButtonDown ("Fire2");
		if(_Fire)
		{
			Fonction_TireFeux();
		}
		// super pouvoir
		_BTsuperPouvoir = Input.GetButtonDown ("Jump");
		if(_BTsuperPouvoir == true)
		{
			Fonction_SuperPouvoir();
		}
		// c**r c**r//
		Fonction_EncadrerPlayer ();

	}
	// deplacement
	void FixedUpdate()
	{
		GetComponent<Rigidbody2D>().velocity = _mouvement;
	}
	//*les Fonctions*//
	// Tire Feux
	private void Fonction_TireFeux()
	{
		Script_Attaquer _Attaquer = GetComponent<Script_Attaquer>();
		if(_Attaquer != null)
		{
			_Attaquer.Attaquer("player");
		}
	}
	// super pouvoir
	private void Fonction_SuperPouvoir()
	{
		if(_tempsEcouler <= 0 )
		{
			_tempsEcouler = _tempsAttente;
			_superPouvoir.enableEmission = true;
			_BTsuperPouvoir = false;
		}
	    
	}
	// collision entre le player et les ennemis :D :D :D
	void OnCollisionEnter2D(Collision2D ennemi)
	{
		if(_tempsEcouler > 0)
		{
			Script_Vie _ennemiScriptVie = ennemi.gameObject.GetComponent<Script_Vie>();
		    if(_ennemiScriptVie != null)
			{
				ennemi.gameObject.GetComponent<Collider2D>().isTrigger = true;
				_ennemiScriptVie._vie = 0;
			}
		}
	}
	// Encadrer le player avec la camera
	private void Fonction_EncadrerPlayer()
	{
		// 6 - Make sure we are not outside the camera bounds
		var dist = (transform.position - Camera.main.transform.position).z;
		var leftBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).x;
		var rightBorder = Camera.main.ViewportToWorldPoint(new Vector3(1, 0, dist)).x;
		var topBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 0, dist)).y;
		var bottomBorder = Camera.main.ViewportToWorldPoint(new Vector3(0, 1, dist)).y;
		//
		transform.position = new Vector3(
			Mathf.Clamp(transform.position.x, leftBorder, rightBorder  ),
			Mathf.Clamp(transform.position.y, topBorder, bottomBorder),
			transform.position.z);
	}

}
