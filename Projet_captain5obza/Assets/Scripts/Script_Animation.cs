﻿using UnityEngine;
using System.Collections;

public class Script_Animation : MonoBehaviour 
{
	public static Script_Animation _instance;
	public ParticleSystem _feu;
	public ParticleSystem _fumer;
	public ParticleSystem _boom;
	public ParticleSystem _etoile;
	/**/
	public ParticleSystem _revolution;
	public ParticleSystem _vive_tunisie_1;
	public ParticleSystem _thawra;
	public ParticleSystem _mois5;
	public ParticleSystem _mois1;

	void Awake()
	{
		if(_instance != null)
		{
			Debug.LogError("Multiple instances of SpecialEffectsHelper!");
		}
		_instance = this;

	}
	//

	//
	public void Mois_1(Vector3 position)
	{
		CreationEffet (_mois1,position);
	}
	public void Mois_5(Vector3 position)
	{
		CreationEffet (_mois5,position);
	}
	public void Collision(Vector3 position)
	{
		CreationEffet (_etoile,position);
	}
	// pour l'explosion !! :D :D :D
	public void ExplosionPlayer(Vector3 position)
	{
		CreationEffet (_boom , position);
		CreationEffet (_feu , position);
	}
	public void ExplosionEnnemie(Vector3 position)
	{
		//CreationEffet (_boom , position);
		CreationEffet (_fumer , position);
		//CreationEffet (_feu , position);
		CreationEffet (_revolution , position);
	}
	public void ImageThawra(Vector3 position)
	{
		CreationEffet (_thawra , position);
	}
	//
	private ParticleSystem CreationEffet(ParticleSystem prefab , Vector3 position)
	{
		ParticleSystem _particleSystem = Instantiate (prefab, position, Quaternion.identity) as ParticleSystem ;
		Destroy (_particleSystem.gameObject , _particleSystem.duration);//startLifetime
		return _particleSystem;
	}
}
