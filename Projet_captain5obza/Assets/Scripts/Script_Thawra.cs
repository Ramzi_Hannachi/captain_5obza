﻿using UnityEngine;
using System.Collections;

public class Script_Thawra : MonoBehaviour 
{

	void Start () 
	{
		StartCoroutine("StartImage");
	}
	
	IEnumerator StartImage()
	{
		yield return new WaitForSeconds (8f);
		Script_Animation._instance.ImageThawra(GetComponent<Transform>().position );
	}

}
