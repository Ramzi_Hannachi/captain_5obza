﻿using UnityEngine;
using System.Collections;

public class Script_Deplacement : MonoBehaviour 
{
	public Vector2 speed = new Vector2(10,10);
	public Vector2 _direction = new Vector2(-1,0);
	private Vector2 mouvement;
	
	void Update()
	{
		mouvement = new Vector2 (speed.x * _direction.x , speed.y * _direction.y);
		
	}
	void FixedUpdate()
	{
		GetComponent<Rigidbody2D>().velocity = mouvement;
	}
}
