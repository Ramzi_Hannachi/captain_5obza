﻿using UnityEngine;
using System.Collections;

public class Script_Invocation : MonoBehaviour
{

	public static void InvocationObject(Transform _Prefab,Transform _Transform)
	{
		//return Instantiate (_Prefab,new Vector2(_Transform.position.x,_Transform.position.y) ,Quaternion.identity) as GameObject;
		var _MissileClone = Instantiate(_Prefab) as Transform;
		_MissileClone.position = _Transform.position;
	}
}
