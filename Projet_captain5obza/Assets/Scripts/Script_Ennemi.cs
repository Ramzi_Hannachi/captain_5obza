﻿using UnityEngine;
using System.Collections;

public class Script_Ennemi : MonoBehaviour 
{
	private Script_Attaquer[] _Attaquers;
	private Script_Deplacement _Deplacement;
	private bool _visible = false;


	// Use this for initialization
	void Awake()
	{
		_Attaquers = GetComponentsInChildren<Script_Attaquer> ();
		_Deplacement = GetComponent<Script_Deplacement> ();
	}
	void Start () 
	{
		NonVisible ();
	}
	// Update is called once per frame
	void Update () 
	{
		if (GetComponent<Renderer>().IsVisibleFrom (Camera.main) == true) 
		{
			Visible ();
			_visible = true;
			foreach(Script_Attaquer  _Attaquer in _Attaquers)
			{
				if(_Attaquer != null)
				{
					if(_Attaquer.MissileDisponible() == true)
					{
						_Attaquer.Attaquer("ennemi");
					}
				}else
				{
					print("ERROR : _Attaquer");
				}
			}
		} 
		else
		{
			if(_visible == true)
			{
				Destroy(gameObject);
			}
		}
	}
	//
	void Visible()
	{
		GetComponent<Collider2D>().enabled = true;
		_Deplacement.enabled = true;
		foreach (Script_Attaquer _Attaquer in _Attaquers)
		{
			_Attaquer.enabled = true;
		}
	}
	//
	void NonVisible()
	{
		GetComponent<Collider2D>().enabled = false;
		_Deplacement.enabled = false;
		foreach (Script_Attaquer _Attaquer in _Attaquers)
		{
			_Attaquer.enabled = false;
		}
	}
}
