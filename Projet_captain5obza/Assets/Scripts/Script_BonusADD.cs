﻿using UnityEngine;
using System.Collections;

public class Script_BonusADD : MonoBehaviour 
{

	private GameObject _player;

	void Start()
	{
		_player = GameObject.Find ("Player");
	}
	void OnTriggerEnter2D(Collider2D _collider)
	{
		if(_collider.gameObject.name=="Player")
		{
			if(Script_GestionBonus._random == 1)
				_player.GetComponent<Script_Vie> ()._vie = _player.GetComponent<Script_Vie> ()._vie + 2;
			if(Script_GestionBonus._random == 2)
				_player.GetComponent<Script_Vie> ()._vie = _player.GetComponent<Script_Vie> ()._vie + 4;
			if(Script_GestionBonus._random == 3)
				_player.GetComponent<Script_Vie> ()._vie = _player.GetComponent<Script_Vie> ()._vie + 1;
			if(Script_GestionBonus._random == 4)
				_player.GetComponent<Script_Vie> ()._vie = _player.GetComponent<Script_Vie> ()._vie + 3;
		}
	}
	void Update()
	{
		Destroy (this.gameObject,2.0f);
	}
}
