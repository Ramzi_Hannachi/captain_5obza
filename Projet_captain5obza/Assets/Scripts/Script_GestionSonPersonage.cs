﻿using UnityEngine;
using System.Collections;

public class Script_GestionSonPersonage : MonoBehaviour 
{
	public static bool _Tuer_BenAli = false;
	public AudioClip _AudioC_BenAliHrab;
	private AudioSource _AudioS_;

	void Awake () 
	{
		_AudioS_ = GetComponent<AudioSource> ();
	}
	void Update () 
	{
		if(_Tuer_BenAli==true)
		{
			_AudioS_.clip = _AudioC_BenAliHrab;
			_AudioS_.Play();
			_Tuer_BenAli = false;
		}
	}
}
