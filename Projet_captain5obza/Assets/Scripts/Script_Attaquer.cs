﻿using UnityEngine;
using System.Collections;

public class Script_Attaquer : MonoBehaviour 
{
	public Transform _MissilePerfab;
	public float _tempsAttente = 0.25f;
	private float _tempsEcouler;

	// Use this for initialization
	void Start () 
	{
		_tempsEcouler = 0f;
	}
	
	// Update is called once per frame
	void Update () 
	{
		if(_tempsEcouler > 0)
		{
			_tempsEcouler -= Time.deltaTime;
		}
	}
	//
	public bool MissileDisponible()
	{
		if (_tempsEcouler <= 0)
						return true;
				else 
						return false;
	}
	//
	public void Attaquer(string proprietaire)
	{
		if(MissileDisponible())
		{
			_tempsEcouler = _tempsAttente;
			var _MissileClone = Instantiate(_MissilePerfab) as Transform;
			_MissileClone.position = transform.position;
			Script_MissileConfig _MissileConfig = _MissileClone.gameObject.GetComponent<Script_MissileConfig>();
			if(_MissileConfig != null)
			{
				_MissileConfig._proprietaire = proprietaire;//importance zéro
			}else
			{
				print("ERROR _MissileConfig");
			}
			Script_Deplacement _Deplacement = _MissileClone.gameObject.GetComponent<Script_Deplacement>();
			if(_Deplacement != null)
			{
				_Deplacement._direction = transform.right;
			}else
			{
				print("ERROR _Deplacement");
			}
		}
	}
}
