﻿using UnityEngine;
using System.Collections;

public class Script_Vie : MonoBehaviour 
{
	public int _vie = 2;
	public int random = 0;
	public string _proprietaire = "player"; // ennemi
	public Script_GestionBonus _Script_GestionBonus;
	public AudioClip _AudioC_Lesion;
	private AudioSource _AudioS_;

	private GameObject _player;
	
	void Start()
	{
		_player = GameObject.Find ("Player");
	}

	void Awake () 
	{
		_AudioS_ = GetComponent<AudioSource> ();
	}

	void OnTriggerEnter2D(Collider2D collider)
	{
		Script_MissileConfig _MissileConfig = collider.GetComponent<Script_MissileConfig>();
	 	if(_MissileConfig != null)
		{
			if(_MissileConfig._proprietaire.Equals(_proprietaire)==false)
			{
				_vie -=_MissileConfig._domage;
				Script_Animation._instance.Collision(transform.position);
				Destroy(collider.gameObject);
				/**/
				if(_proprietaire=="player")
				{
					_AudioS_.clip = _AudioC_Lesion;
					_AudioS_.Play();
					Script_Animation._instance.Mois_1(transform.position);
				}
		
				/**/
			}
			if(_vie <= 0)
			{
				if(_proprietaire=="player")
				{
					Script_Animation._instance.ExplosionPlayer(transform.position);
				}else
				{
					if(this.name.Equals("benali"))
						Script_GestionSonPersonage._Tuer_BenAli = true;
					Script_Animation._instance.ExplosionEnnemie(transform.position);
					//
					_Script_GestionBonus.Fonction_GestionBonus(Random.Range(1, 4),this.transform);
				}
				Destroy(gameObject);
			}
		}
	}


}
