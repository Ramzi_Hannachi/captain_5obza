﻿using UnityEngine;
using System.Collections;

public class Script_CollisionExplosion : MonoBehaviour
{
	
	void OnCollisionEnter2D(Collision2D collision)
	{
		if(collision.gameObject.name=="Player")
		{
			Script_Animation._instance.ExplosionEnnemie(transform.position);
			Destroy(gameObject);
			//Script_Animation._instance.ExplosionPlayer(transform.position);
			//Destroy(collision.gameObject);
			collision.gameObject.GetComponent<Script_Vie>()._vie = 
				collision.gameObject.GetComponent<Script_Vie>()._vie - 5;
			Script_Animation._instance.Mois_5(transform.position);
		}
	}	
}
